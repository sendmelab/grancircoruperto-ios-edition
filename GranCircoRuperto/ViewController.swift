//
//  ViewController.swift
//  GranCircoRuperto
//
//  Created by Carlos Maldonado G. on 9/4/18.
//  Copyright © 2018 Sendmepro. All rights reserved.
//

import UIKit
import UserNotifications
import AudioToolbox

class informationSample : UIViewController {
    @IBAction func push(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        let jeremyGif = UIImage.gifImageWithName("stars")
        let imageView = UIImageView(image: jeremyGif)
        imageView.frame = CGRect(x: 0.0, y: 59.0, width: self.view.frame.size.width, height: self.view.frame.size.height - 50)
        imageView.alpha = 0.17
        view.addSubview(imageView)
    }
}
class ViewController: UIViewController {
    var isWelcome = false
    //@IBOutlet weak var moption: UIToolbar!
    //@IBOutlet weak var cat: WKWebView!
    @IBOutlet weak var japi: UIStackView!
    //@IBOutlet weak var pop: WKWebView!
    //@IBOutlet weak var trace: UIImageView!
    @IBOutlet weak var share_func: UIBarButtonItem!
    @IBAction func open_face(_ sender: UIButton) {
        openSocialUrl(lua: 0)
    }
    @IBAction func open_twit(_ sender: UIButton) {
        openSocialUrl(lua: 1)
    }
    @IBAction func open_pint(_ sender: UIButton) {
        openSocialUrl(lua: 2)
    }
    @IBAction func open_inst(_ sender: UIButton) {
        openSocialUrl(lua: 3)
    }
    /*@IBAction func funciones(_ sender: UIButton) {
        showAndHideFunc(inShow: true)
    }*/
    @IBAction func exitFrame(_ sender: UIBarButtonItem) {
        showAndHideFunc(inShow: false)
    }
    /*@IBAction func shareBetaOption(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Compartir Promo", message: "¿Quieres compartir la promoción con tus amigos y familiares de El Gran Circo de Ruperto?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Compartir", comment: "Share action"), style: .default, handler: { _ in
            var parse : UIImage!
            do {
                let urls = URL(string: "https://elgrancircoderuperto.cl/app/banner/banner.png")
                let data = try Data(contentsOf: urls!)
                parse = UIImage(data: data)
                let actionVC = UIActivityViewController(activityItems: [parse], applicationActivities: nil)
                actionVC.popoverPresentationController?.sourceView = self.view
                self.present(actionVC, animated: true, completion: nil)
            }
            catch{
                print(error)
                let alert = UIAlertController(title: "Error al compartir", message: "Ha ocurrido un error de conexión, por favor intentelo nuevamente.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Abort"), style: .cancel, handler: { _ in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancelar", comment: "Abort"), style: .cancel, handler: { _ in
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }*/
    
    private func showTraceDistribution(){
        //trace.isHidden = false
        //moption.isHidden = false
    }
    private func showAndHideFunc(inShow: Bool){
        //cat.isHidden = !inShow
        //pop.isHidden = !inShow
        //moption.isHidden = !inShow
        japi.isHidden = inShow
        //trace.isHidden = true
        if (inShow){
            /*cat.load(URLRequest(url: URL(string: "https://elgrancircoderuperto.cl/app/banner/banner.html")!))
            pop.load(URLRequest(url: URL(string: "https://elgrancircoderuperto.cl/app/funciones/")!))*/
        }
    }
    
    private func openSocialUrl(lua: Int8){
        var act:String = "https://m.facebook.com/CircodeRuperto/"
        switch lua {
        case 1:
            act = "https://mobile.twitter.com/CircoRuperto"
            break
        case 2:
            act = "https://www.pinterest.cl/circoderuperto"
            break
        case 3:
            act = "https://www.instagram.com/elgrancircoderuperto/"
            break
        default:
            break
        }
        
        guard let url = URL(string: act) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let jeremyGif = UIImage.gifImageWithName("stars")
        let imageView = UIImageView(image: jeremyGif)
        imageView.frame = CGRect(x: 0.0, y: 20.0, width: self.view.frame.size.width, height: self.view.frame.size.height-20)
        imageView.alpha = 0.11
        view.addSubview(imageView)
        
        //NOTIFICATION MODULE LOADER
        pushNotificationSender()
    }
    private func pushNotificationSender(){
        let contenido = UNMutableNotificationContent()
        contenido.title = "Bienvenido"
        contenido.body = "Las ultimas novedades y promociones se mostraran aqui cuando esten disponibles."
        contenido.sound = UNNotificationSound.default()
        
        let displayer = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        let request = UNNotificationRequest(identifier: "makingRemotePush", content: contenido, trigger: displayer)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        /*if (self.isWelcome == false){
            let alert = UIAlertController(title: "Bienvenido", message: "La aplicación de El Gran Circo de Ruperto les da la bienvenida.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("Received.")
            }))
            self.present(alert, animated: true, completion: nil)
            self.isWelcome = true
        }*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

