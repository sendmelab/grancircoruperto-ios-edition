
import Foundation
import WebKit
import MapKit
import CoreLocation

class funcionModeSamples : UIViewController {
    @IBOutlet weak var webFunciones: WKWebView!
    @IBOutlet weak var pictureFrame: UIImageView!
    @IBAction func refreshFunctions(_ sender: Any) {
        reloadSuperFuncion()
    }
    private func reloadSuperFuncion(){
        do {
            //LOADING BANNER FRAME
            if (webFunciones.isHidden){
                webFunciones.isHidden = false
            }
            let urls = URL(string: "https://elgrancircoderuperto.cl/app/banner/banner.png")
            let data = try Data(contentsOf: urls!)
            pictureFrame.image = UIImage(data: data)
            //LOADING WEBPAGE FRAME
            webFunciones.load(URLRequest(url: URL(string: "https://elgrancircoderuperto.cl/app/funciones/")!))
        }
        catch{
            print(error)
            self.webFunciones.isHidden = true
            let alert = UIAlertController(title: "Error al cargar", message: "No hay conexión a internet por favor vuelva a intentarlo más tarde.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Abort"), style: .cancel, handler: { _ in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        reloadSuperFuncion()
    }
    /*
    @IBOutlet weak var pictureFrame: UIImageView!
    
    @IBAction func exitNode(_ sender: UIBarButtonItem) {
     
    }
    @IBOutlet weak var embedFunc: WKWebView!
 */
}

class RupertoWeb : UIViewController{
    @IBOutlet weak var nom: WKWebView!
    //@IBOutlet weak private var maps: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nom.load(URLRequest(url: URL(string: "https://elgrancircoderuperto.cl/app/ubicacion/ubicacion.html")!))
        //mapViewer()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    /*private func mapViewer(){
        let location = CLLocationCoordinate2DMake(-32.834026, -70.599543)
        let spa = MKCoordinateSpanMake(0.005, 0.017)
        let region = MKCoordinateRegionMake(location, spa)
        maps.setRegion(region, animated: true)
        maps.mapType = .standard
        /*let anotacion = MKPointAnnotation()
        anotacion.coordinate = location
        anotacion.title = "Circo Ruperto"
        maps.addAnnotation(anotacion)*/
    }*/
}

class RupertoValores : UIViewController{
    @IBOutlet weak var valor: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        valor.load(URLRequest(url: URL(string: "https://elgrancircoderuperto.cl/app/valores/")!))
    }
}
