//
//  DialogSet.swift
//  GranCircoRuperto
//
//  Created by commander on 26-12-18.
//  Copyright © 2018 SendMepro. All rights reserved.
//
import UIKit

class DialogSet {
    typealias UIDial = (UIAlertController?) -> Void
    
    func regularUIDialog (tit: String, titMsg: String, capture : @escaping UIDial){
        let dialog = UIAlertController(title: tit, message: titMsg, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Aceptar", style:.default, handler: nil)
        dialog.addAction(ok)
        capture(dialog)
    }
}
