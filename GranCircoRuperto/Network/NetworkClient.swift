//
//  NetworkClient.swift
//  GranCircoRuperto
//
//  Created by commander on 19-12-18.
//  Copyright © 2018 SendMepro. All rights reserved.
//
import Foundation
import Alamofire
//import SwiftyJSON

class NetworkClient {
    typealias WebServiceResponse = ([[String: Any]]?, Error?) -> Void
    typealias jsonFragment = (NSDictionary?, Error?)-> Void
    typealias externalResp = (NSDictionary?, Int?) -> Void
    
    func execute (_urlClient : URL, success : @escaping WebServiceResponse){
        Alamofire.request(_urlClient).validate().responseJSON{
            response in if let error = response.error {
                success(nil, error)
            }else if let jsonArray = response.result.value as? [[String: Any]]{
                success(jsonArray, nil)
            }else if let jsonDict = response.result.value as? [String: Any]{
                success([jsonDict], nil)
            }
        }
    }
    
    func post_request_callback (urmode: String, params: [String:Any], cause: @escaping externalResp){
        //RETORNOS CON REQUEST NO CON POST
        guard let uri = URL(string: "https://elgrancircoderuperto.cl/app/pub_access/aps.php/\(urmode)") else { return }
        Alamofire.request(uri, parameters: params, encoding: URLEncoding(destination: .queryString), headers: nil).validate().responseJSON {
            response in
            switch response.result {
            case .success:
                /*let fromVal = response.value as! NSDictionary
                print(fromVal.value(forKey: "error_int") as Any)*/
                //
                cause(response.value as? NSDictionary, 1)
                break
            case .failure(let error):
                print(error.localizedDescription)
                cause(nil, -1)
            }
        }
    }
    
    func saveAndRemoveCredentials (isRemove : Bool, setCreds : [String : Any]){
        if isRemove {
            if (UserDefaults.standard.object(forKey: "privateCred") != nil){
                UserDefaults.standard.removeObject(forKey: "privateCred")
                print("PRIVATE CREDENTIALS AS BEEN REMOVED ON YOUR DEVICE")
            }else{
                print("NONE TO REMOVE AT THIS TIME")
            }
        }else{
            if setCreds.count > 0 {
                if (UserDefaults.standard.object(forKey: "privateCred") == nil){
                    UserDefaults.standard.set(setCreds, forKey: "privateCred")
                    print("PRIVATE CREDENTIALS SAVED SUCCESSFULLY")
                }
            }
        }
    }
    
    func execJSONP (uri : URL, success : @escaping jsonFragment) {
        Alamofire.request(uri).validate().responseJSON {
            response in if let error = response.error {
                success(nil, error)
            }else if let jsonDetect = response.result.value {
                if let json = try? jsonDetect as! NSDictionary {
                    /*for item in json["people"].arrayValue {
                        print(item["firstName"].stringValue)
                    }*/
                    success(json, nil)
                }
            }
        }
    }
}
