//
//  Reserv2.swift
//  GranCircoRuperto
//
//  Created by commander on 06-02-19.
//  Copyright © 2019 SendMepro. All rights reserved.
//

import Foundation
import UIKit

class Reserv2 : UIViewController  {
    private var netService = NetworkClient()
    private var netDialogue = DialogSet()
    private var paymentAvaitable:Bool = false
    private static var netIDRespond:Int!
    private var netExternalExecute:Timer!
    //LISTADO DE ACCIONES
    @IBOutlet weak var paymentAction: UIBarButtonItem!
    @IBOutlet weak var refreshTable: UIBarButtonItem!
    @IBOutlet weak var runuser: UILabel!
    @IBOutlet weak var regularTableView: UITableView!
    //()=[]
    
    private func realizacionDePago(paypal:Bool) {
        if (UserDefaults.standard.object(forKey: "privateCred") != nil){
            let user:[String:Any] = (UserDefaults.standard.object(forKey: "privateCred")) as! [String:Any]
            netService.post_request_callback(urmode: "?mode=pass", params: ["run":String(user["USER"] as! String),"psw":String(user["PASS"] as! String)]) {
                (jp,err) in
                if (err != 0) {
                    guard let url = URL(string: "https://elgrancircoderuperto.cl/app/pub_access/aps.php/ticket?setup=\(String(user["USER"] as! String))\(paypal ? "&PP=PayPal":"")") else {
                        return
                    }
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
                self.isNowReady(readyNow: true)
            }
        }
        
    }
    @IBAction func recargar_lista(_ sender: UIBarButtonItem) {
        isNowReady(readyNow: false)
        dataUpdater()
    }
    @IBAction func pagare(_ sender: UIBarButtonItem) {
        let aviso = UIAlertController(title: "Pago", message: "Por favor seleccione una forma de pago para continuar.", preferredStyle: .alert)
        let cno = UIAlertAction(title: "Cancelar", style:.cancel, handler: nil)
        let cpay = UIAlertAction(title: "Pagar con PayPal", style:.default, handler: {_ in
            self.isNowReady(readyNow: false)
            self.realizacionDePago(paypal: true)
        })
        aviso.addAction(cpay)
        aviso.addAction(cno)
        self.present(aviso, animated: true, completion: nil)
    }
    var tempSample:[Coll] = []
    @objc private func net_externalExec() {
        if (Reserv2.netIDRespond != nil) {
            let ID_buy:Int = Reserv2.netIDRespond
            Reserv2.netIDRespond = nil
            let aviso = UIAlertController(title: "Confirmación", message: "¿Estas seguro que quieres anular esta compra sin confirmar?.", preferredStyle: .alert)
            let cno = UIAlertAction(title: "Cancelar", style:.cancel, handler: nil)
            let cyes = UIAlertAction(title: "Eliminar", style:.destructive, handler: {_ in
                self.isNowReady(readyNow: false)
                //OPCION DE ANULACION
                let pam:[String:Any] = ["value_0":self.runuser.text!,"value_1":String(ID_buy),"cuvalue":[false,true]]
                self.netService.post_request_callback(urmode: "?mode=exp", params: ["launch":"ticketCancelAction","AM":pam]) {
                    (spe,err) in
                    if (err==0) {
                        
                    }else{
                        let tec:Int8 = Int8((spe?.value(forKey: "error_int") as! NSNumber).int8Value)
                        if (tec != 1) {
                            self.netDialogue.regularUIDialog(tit: "Error", titMsg: "No se pudo completar la accion, es posible que esta compra este en revisión.") {
                                (spa) in
                                self.present(spa!, animated: true, completion: nil)
                            }
                        }
                    }
                    self.dataUpdater()
                }
            })
            aviso.addAction(cno)
            aviso.addAction(cyes)
            self.present(aviso, animated: true, completion: nil)
        }
    }
    func totalArray_update (stack: [[String:Any]]) -> [Coll] {
        tempSample = []
        if (stack.count > 0) {
            for muu in 1...stack.count {
                let campo = Coll(labels: stack[muu-1]["ticket"] as! String, pay_action: stack[muu-1]["ID"] as! Int, ima:  (stack[muu-1]["forma"] as! String)=="paypal" ?  #imageLiteral(resourceName: "pp_mini"): #imageLiteral(resourceName: "va_mini"), abort: stack[muu-1]["pago"] as! Bool)
                tempSample.append(campo)
            }
        }else{
            netDialogue.regularUIDialog(tit: "Vacío", titMsg: "Aun no has realizado una compra.") {
                (dial) in
                self.present(dial!, animated: true, completion: nil)
            }
        }
        regularTableView.reloadData()
        return tempSample
    }
    
    //@IBOutlet weak var actualStatus: UIStackView!
    @IBAction func exitList(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) {
            self.netExternalExecute.invalidate()
        }
    }
    static func cancelacion(item_key: Int) {
        Reserv2.netIDRespond = item_key
    }
    func ext_viewer(advice: UIAlertController) {
        present(advice, animated: true, completion: nil)
    }
    
    //var sample = [String]()
    private func dataUpdater(){
        regularTableView.tableFooterView = UIView.init(frame: .zero)
        paymentAvaitable = false
        var coleccion:NSArray!
        var cosa:[[String:Any]] = []
        netService.post_request_callback(urmode: "?mode=load", params: ["launch":"ticketViewer","AM":["wh":["Rut"],"dd":[runuser.text!],"ty":[true],"ao":[false]]]) {
            (opa,err) in
            if err==0 {
                
            }else{
                if (opa != nil) {
                    let epo = opa?.value(forKey: "error_int") as! Int
                    if (epo == 0) {
                        coleccion = opa?.value(forKey: "VALOR") as! NSArray
                        //EXPERIMENTO AÑADIR ELEMENTOS DINAMICOS
                        if (coleccion.count > 0) {
                            for nt in 1...coleccion.count {
                                let paystatus:Bool = (((coleccion[nt-1]) as AnyObject).value(forKey: "Pagado") as! NSString).boolValue
                                var totalvalue:Double = (((coleccion[nt-1]) as AnyObject).value(forKey: "Valor_a_pagar") as! NSString).doubleValue
                                let cantidad:Int8 = Int8((((coleccion[nt-1]) as AnyObject).value(forKey: "POSTICKET") as! NSString).intValue)
                                totalvalue = (totalvalue*Double(cantidad))
                                let order_sam:AnyObject = (((coleccion[nt-1]) as AnyObject).value(forKey: "ORDER_INVOICE") as AnyObject)
                                var order_sam_str:String = "Sin Recibo"
                                if !(order_sam is NSNull) {
                                    order_sam_str = String(order_sam as! String)
                                }
                                let ticket_sample:String = "Ubicacion: \(((coleccion[nt-1]) as AnyObject).value(forKey: "Membresia") as! String)\nEstado: \(paystatus ? "Completado":"Pago en espera")\nRecibo: \(order_sam_str)\nValor a pagar: \(String(totalvalue)+" "+(((coleccion[nt-1]) as AnyObject).value(forKey: "Moneda") as! String))\(cantidad > 1 ? "\n"+String(cantidad)+" Tickets":"")"
                                cosa.append(["ticket":ticket_sample,"pago":paystatus,"ID":Int((((coleccion[nt-1]) as AnyObject).value(forKey: "ID") as! NSString).intValue),"forma":(((coleccion[nt-1]) as AnyObject).value(forKey: "Forma_de_pago") as! String).lowercased()])
                                if (!paystatus) {
                                    self.paymentAvaitable = true
                                }
                            }
                        }
                    }
                    self.tempSample = self.totalArray_update(stack: cosa)
                }else{
                    
                }
                self.isNowReady(readyNow: true)
            }
        }
    }
    private func isNowReady(readyNow: Bool) {
        paymentAction.isEnabled = paymentAvaitable ? readyNow:false
        refreshTable.isEnabled = readyNow
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isNowReady(readyNow: false)
        if (UserDefaults.standard.object(forKey: "privateCred") != nil){
            let users:[String:Any] = (UserDefaults.standard.object(forKey: "privateCred")) as! [String:Any]
            runuser.text = users["USER"] as! String
            dataUpdater()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        netExternalExecute = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(net_externalExec), userInfo: nil, repeats: true)
        regularTableView.delegate = self
        regularTableView.dataSource = self
        
    }
}
extension Reserv2: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempSample.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let samples = tempSample[indexPath.row]
        let cells = tableView.dequeueReusableCell(withIdentifier: "Vision") as! VistaCell
        
        cells.setAnyValues(onion: samples)
        
        return cells
    }
}
