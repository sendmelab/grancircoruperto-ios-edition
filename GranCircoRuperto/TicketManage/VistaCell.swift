//
//  VistaCell.swift
//  GranCircoRuperto
//
//  Created by commander on 07-02-19.
//  Copyright © 2019 SendMepro. All rights reserved.
//

import UIKit

class Coll{
    var labels: String
    var pay_id: Int
    var image_selection: UIImage
    var isAbortAction: Bool
    
    init(labels: String, pay_action: Int, ima: UIImage, abort: Bool) {
        self.labels = labels
        self.pay_id = pay_action
        self.image_selection = ima
        self.isAbortAction = !abort
    }
}

class VistaCell : UITableViewCell {
    @IBOutlet weak var action0: UILabel!
    @IBOutlet weak var imas: UIImageView!
    @IBOutlet weak var cancelar_orden: UIButton!
    //DELEGACION DE VARIABLES
    func setAnyValues (onion: Coll) {
        action0.text = onion.labels
        //cancelar_orden.setTitle(onion.pay_action, for: .normal)
        cancelar_orden.tag = onion.pay_id
        cancelar_orden.addTarget(self, action: #selector(cancelar_pago), for: .touchUpInside)
        cancelar_orden.setTitleColor(onion.isAbortAction ? UIColor.red:UIColor.darkGray, for: .normal)
        cancelar_orden.isEnabled = onion.isAbortAction
        imas.image = onion.image_selection
    }
    @objc func cancelar_pago(sender: UIButton!) {
        Reserv2.cancelacion(item_key: sender.tag)
    }
}

