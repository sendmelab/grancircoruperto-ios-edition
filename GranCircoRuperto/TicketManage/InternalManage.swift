//
//  InternalManage.swift
//  GranCircoRuperto
//
//  Created by commander on 08-01-19.
//  Copyright © 2019 SendMepro. All rights reserved.
//
import UIKit
import Foundation

class Reservas : UIViewController {
    private var semolasJS = NetworkClient()
    private var dialogos = DialogSet()
    private var uri_indicator_mode:String = "https://elgrancircoderuperto.cl/app/pub_access/aps.php/tickauth/appdolar"
    //SELECCION DE LUGARES
    private var lugar:Int8 = 0
    private var lugar_prices:[String:NSString]!
    private var disponibilidad:[String:NSString]!
    private var payfee:Int8!
    private var payment_selected:Int8!
    var ubicaciones:[Int8] = [0,0,1]
    private var dollar_currency:Double!
    private var selected_item:String!
    @IBOutlet weak var viewer: UIView!
    //IMAGEN DE SELECCION
    @IBOutlet weak var palcoSeleccionado: UIImageView!
    @IBOutlet weak var plateaSeleccionado: UIImageView!
    @IBOutlet weak var tribunaSeleccionado: UIImageView!
    //BOTONES DE ACCION
    @IBOutlet weak var plateaBtn: UIButton!
    @IBOutlet weak var tribunaBtn: UIButton!
    @IBOutlet weak var palcoBtn: UIButton!
    @IBOutlet weak var reservaBtn: UIButton!
    @IBOutlet weak var compras: UIBarButtonItem!
    
    //TEXTOS DE EVENTO SELECT
    @IBOutlet weak var pesito: UILabel!
    @IBOutlet weak var zonaSelect: UILabel!
    @IBOutlet weak var disponibleCount: UILabel!
    @IBOutlet weak var counterTicket: UILabel!
    @IBOutlet weak var pesito_resumen: UILabel!
    //ACCION BOTON
    @IBOutlet weak var closingAction: UIButton!
    @IBOutlet weak var messageLoader: UILabel!
    @IBOutlet weak var moreTicket: UIStepper!
    
    private func evento_pagar() {
        if (payment_selected != nil && selected_item != nil) {
            if ((disponibilidad[selected_item])?.integerValue)! > 0 {
                var precio_final:Double!
                let cel_precio = ((lugar_prices[selected_item])?.intValue)!
                if (payment_selected == 2) {
                    precio_final = ((Double(cel_precio)/dollar_currency))*(1.000+((0.054*Double(payfee))/100))+(0.3+((Double(cel_precio)/dollar_currency)/200))
                    precio_final = Double(String(format: "%.2f", ceil(precio_final*100)/100))
                    //precio_final = precio_final*moreTicket.value
                    pesito_resumen.text = "Precio Final: $ \(String(precio_final)) USD \(moreTicket.value > 1 ? "(x"+String(Int(moreTicket.value))+")":"") con PayPal - Valor Actual en CLP: $ \(String(dollar_currency)) CLP"
                }else {
                }
            }else {
                pesito_resumen.text = "No esta disponible para comprar en este momento."
            }
            reservaBtn.isEnabled = (payment_selected != nil && ((disponibilidad[selected_item])?.integerValue)! > 0)
        }
    }
    private func isHiddingMainActions(hidding: Bool){
        closingAction.isEnabled = !hidding
        viewer.isHidden = hidding
        compras.isEnabled = !hidding
    }
    private func asignacionDeReserva(auto_pay:Bool){
        if (UserDefaults.standard.object(forKey: "privateCred") != nil) {
            var selection_num:Int!
            let users:[String:Any] = (UserDefaults.standard.object(forKey: "privateCred")) as! [String:Any]
            var price:Double = ((lugar_prices[selected_item])?.doubleValue)!
            if payment_selected == 2 {
                price = ((price/dollar_currency))*(1.000+((0.054*Double(payfee))/100))+(0.3+((price/dollar_currency)/200))
                price = Double(String(format: "%.2f", ceil(price*100)/100))!
            }
            switch(selected_item){
            case .none: break
                
            case "palcos":
                selection_num = 5
                break
            case "pla_adultos":
                selection_num = 4
                break
            case "adultos":
                selection_num = 2
                break
            case .some(_): break
            }
            let fl:[String:Any] = ["value_0":users["USER"]!,"value_1":Int(moreTicket.value),"value_2":payment_selected,"value_3":price,"value_4":payment_selected,"value_5":"null","value_6":selection_num,"cuvalue":[false,true,true,true,true,true,true]]
            semolasJS.post_request_callback(urmode: "?mode=exp", params: ["launch":"ticketApplyBeforeBuy","AM":fl]) {
                (jo, error) in
                var errorAppear:Bool = false
                var mess:String!
                if (error == 0) {
                    errorAppear = true
                    mess = "Error al generar la reserva, por favor reintente nuevamente."
                } else {
                    if (jo != nil) {
                        let coll:NSDictionary = jo!.value(forKey: "collect") as! NSDictionary
                        if (coll != nil) {
                            let bb:Int8 = Int8((coll.value(forKey: "status") as! NSString).intValue)
                            if (bb==1) {
                                if (auto_pay) {
                                    self.messageLoader.text = "Generando codigo de acceso express..."
                                    self.semolasJS.post_request_callback(urmode: "?mode=pass", params: ["run":users["USER"]!,"psw":users["PASS"]!]) {
                                        (jp,err) in
                                        if (err != 0) {
                                            guard let url = URL(string: "https://elgrancircoderuperto.cl/app/pub_access/aps.php/ticket?setup=\(String(users["USER"] as! String))\(self.payment_selected == 2 ? "&PP=PayPal":"")") else {
                                                return
                                            }
                                            if #available(iOS 10.0, *) {
                                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                            } else {
                                                UIApplication.shared.openURL(url)
                                            }
                                        }
                                        self.messageLoader.text = "Please Wait."
                                        self.isHiddingMainActions(hidding: false)
                                    }
                                }else{
                                    errorAppear = true
                                    mess = "Estimado cliente, se acaba de aplicar una reserva de ticket que se encuentra en estado Pendiente de Pago, recuerde que para hacer uso de este ticket debe pagar el valor estipulado anteriormente."
                                }
                            }else {
                                errorAppear = true
                                mess = "La compra ya existe o no pudo ser procesada correctamente."
                            }
                        }else{
                            errorAppear = true
                            mess = "No se encontro el servicio esperado."
                        }
                    }else{
                        errorAppear = true
                        mess = "Se interrumpio la conexion con el servidor, por favor asegurece que se encuentre conectado a internet e intentelo nuevamente."
                    }
                    if (errorAppear) {
                        self.dialogos.regularUIDialog(tit: "Atencion", titMsg: mess){
                            (diag) in
                            self.present(diag!, animated: true, completion: nil)
                        }
                        self.isHiddingMainActions(hidding: false)
                    }else{
                        self.reloadPalcoAndValues()
                    }
                }
            }
        }else{
            closing_session_log()
        }
    }
    private func verifyUrl (urlString: String?) -> Bool {
        //Check for nil
        if let urlString = urlString {
            // create NSURL instance
            if let url = NSURL(string: urlString) {
                // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    @IBAction func reservar_action(_ sender: UIButton) {
        let aviso = UIAlertController(title: "Confirmación", message: "Estas a punto de confirmar esta compra, durante este momento puedes realizar el pago ahora o aplicar una reserva y pagarlo más adelante.", preferredStyle: .alert)
        let apply = UIAlertAction(title: NSLocalizedString("Pagar ahora", comment: "Reasignar"), style: .default, handler: { _ in
            self.isHiddingMainActions(hidding: true)
            self.asignacionDeReserva(auto_pay: true)
        })
        let applyNoPay = UIAlertAction(title: NSLocalizedString("Pagar más tarde", comment: "Reasignar"), style: .default, handler: { _ in
            self.isHiddingMainActions(hidding: true)
            self.asignacionDeReserva(auto_pay: false)
        })
        let cancel = UIAlertAction(title: "Cancelar", style:.cancel, handler: nil)
        aviso.addAction(apply)
        aviso.addAction(applyNoPay)
        aviso.addAction(cancel)
        self.present(aviso, animated: true, completion: nil)
        
    }
    
    @IBAction func paySelected(_ sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == 1) {
            payment_selected = 1
        }else {
            payment_selected = 2
        }
        evento_pagar()
    }
    
    @IBAction func applyExtraTickets(_ sender: UIStepper) {
        if selected_item != nil {
            let ticketCount:Int = Int(sender.value)
            let precio:Int = ((lugar_prices[selected_item])?.integerValue)!
            counterTicket.text = String(Int(ticketCount))
            pesito.text = "$ \(String(precio*ticketCount))"
            evento_pagar()
            var contador:Int = ((disponibilidad[selected_item])?.integerValue)!
            let contador_ff:Int = contador
            if (contador > 0) {
                contador = (contador-Int(moreTicket.value))
                disponibleCount.text = (contador_ff > Int(moreTicket.value) ? "\(String(contador)) en total":"No hay más tickets")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    private func mostrarYReintentarNuevamente (){
        let service = UIAlertController(title: "Atención", message: "Parece que tenemos problemas de conexión, reintente nuevamente o cierre la sesion.", preferredStyle: .alert)
        let retryAction = UIAlertAction(title: NSLocalizedString("Reintentar", comment: "retry"), style: .default, handler: { _ in
            self.reloadPalcoAndValues()
        })
        let closing = UIAlertAction(title: NSLocalizedString("Cerrar Sesión", comment: "close"), style: .cancel, handler: { _ in
            self.closing_session_log()
        })
        service.addAction(retryAction)
        service.addAction(closing)
        self.present(service, animated: true, completion: nil)
    }
    
    @IBAction func pushSelection(_ sender: UIButton) {
        if !sender.isSelected {
            plateaBtn.isSelected = false
            palcoBtn.isSelected = false
            tribunaBtn.isSelected = false
            sender.isSelected = true
            zonaSelect.text = sender.titleLabel?.text?.uppercased()
            plateaSeleccionado.isHidden = !plateaBtn.isSelected
            palcoSeleccionado.isHidden = !palcoBtn.isSelected
            tribunaSeleccionado.isHidden = !tribunaBtn.isSelected
            switch(sender.titleLabel?.text?.lowercased()){
            case .none: break
                
            case "palco":
                selected_item = "palcos"
                break
            case "platea":
                selected_item = "pla_adultos"
                break
            case "tribuna":
                selected_item = "adultos"
                break
            case .some(_): break
            }
            pesito.text = "$ \(String(lugar_prices[selected_item]!))"
            let distribu:Int = ((disponibilidad[selected_item])?.integerValue)!
            if distribu > 0 {
                moreTicket.maximumValue = Double(distribu)
                moreTicket.value = 1
            }
            counterTicket.text = (distribu > 0 ? String(moreTicket.value) : "--")
            disponibleCount.text = distribu > 0 ? "\(String(Int(distribu)-1)) en total" : "No disponible"
            moreTicket.isEnabled = distribu > 0
            evento_pagar()
        }
    }
    
    @IBAction func close_session(_ sender: UIButton) {
        closing_session_log()
    }
    
    private func closing_session_log (){
        if closingAction.isEnabled {
            closingAction.isEnabled = false
        }
        if !viewer.isHidden {
            viewer.isHidden = true
        }
        messageLoader.text = "Logging Out..."
        semolasJS.saveAndRemoveCredentials(isRemove: true, setCreds: [:])
        guard let logout:URL = URL(string: "https://elgrancircoderuperto.cl/app/pub_access/?mode=out") else{
            return
        }
        semolasJS.execJSONP(uri: logout) { (enn, err) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var valueUser: UILabel!
    
    private func reloadPalcoAndValues (){
            guard let valores = URL(string: "https://elgrancircoderuperto.cl/app/valores/value.json") else {
                return
            }
            guard let dolarToday:URL = URL(string: uri_indicator_mode) else{
                return
            }
            semolasJS.execJSONP(uri: valores){
                (koko,err) in
                if let err = err {
                    print(err.localizedDescription)
                    self.mostrarYReintentarNuevamente()
                }else if let koko = koko {
                    self.payfee = Int8((koko.value(forKey: "paypal_fee") as! NSString).intValue)
                    self.lugar_prices = koko.value(forKey: "single_value") as! [String:NSString]
                    self.disponibilidad = koko.value(forKey: "disponible") as! [String:NSString]
                    self.semolasJS.execJSONP(uri: dolarToday){
                        (dol, errAgain) in
                        if let errAgain = errAgain {
                            print(errAgain.localizedDescription)
                            self.mostrarYReintentarNuevamente()
                            self.uri_indicator_mode = "https://elgrancircoderuperto.cl/app/pub_access/aps.php/tickauth/appdolar"
                        }else if let dol = dol {
                            let commns:NSDictionary = dol.value(forKey: "moneda") as! NSDictionary
                            if commns.count > 0{
                                self.dollar_currency = (commns.value(forKey: "dolar") as! NSString).doubleValue
                            }
                            self.isHiddingMainActions(hidding: false)
                            
                            //self.ubicaciones = [0]
                            //Config
                        }
                    }
                }
            }
    }
    
    override func viewDidLoad() {
        if (UserDefaults.standard.object(forKey: "privateCred") != nil){
            let users:[String:Any] = (UserDefaults.standard.object(forKey: "privateCred")) as! [String:Any]
            valueUser.text = users["USER"] as! String
            
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        isHiddingMainActions(hidding: true)
        reloadPalcoAndValues()
    }
}

