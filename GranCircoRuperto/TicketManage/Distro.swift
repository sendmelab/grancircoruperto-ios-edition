//
//  Distro.swift
//  GranCircoRuperto
//
//  Created by Whoz on 16-12-18.
//  Copyright © 2018 SendMepro. All rights reserved.
//
import Alamofire
import SwiftyJSON

class distroPurchase : UIViewController {
    private var regionServerPhone:String = "NA"
    private var regionFee:[String] = []
    private var regionSelected:Int = 0
    private let jsonClient = NetworkClient()
    private var readyAndApply = false
    private var isReallyLoaded : Bool = false
    private let exDialog = DialogSet()
    
    @IBOutlet weak var Advicer: UILabel!
    
    @IBOutlet weak var runField: UITextField!
    @IBOutlet weak var UIStatus: UILabel!
    @IBOutlet weak var plabel: UILabel!
    @IBOutlet weak private var registro: UIButton!
    @IBOutlet weak private var inicio: UIButton!
    @IBOutlet weak var progress: UIActivityIndicatorView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var registro_UI: UIView!
    @IBOutlet weak var secondRegisterID: UIScrollView!
    
    //FONDO DE TEXTOS
    @IBOutlet weak var full_name: UITextField!
    @IBOutlet weak var email_field: UITextField!
    @IBOutlet weak var phone_field: UITextField!
    @IBOutlet weak var country_select: UIPickerView!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var contrySelection: UIButton!
    @IBAction func pushToSelectCountry(_ sender: UIButton) {
        if (!sender.isHidden){
            sender.isHidden = true
            country_select.isHidden = false
        }
    }
    
    @IBAction func runBeenChanged(_ sender: UITextField) {
        if !secondRegisterID.isHidden {
            secondRegisterID.isHidden = true
            registro.isEnabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registro.layer.cornerRadius = 10
        registro.clipsToBounds = true
        inicio.layer.cornerRadius = 10
        inicio.clipsToBounds = true
        country_select.dataSource = self
        country_select.delegate = self
        let locale = Locale.current
        regionServerPhone = locale.regionCode as! String
        Advicer.text = (regionServerPhone != "CL" ? "Para comenzar primero ingrese su nombre de usuario comun para comenzar con el registro y verifique que este disponible.":"Para comenzar primero necesitamos registrar su RUT para comenzar con el registro.")
        runField.keyboardType = (regionServerPhone != "CL" ? UIKeyboardType.default:UIKeyboardType.numberPad)
        runField.placeholder = (regionServerPhone != "CL" ? "Ingresar nombre de usuario":"Ingrese su R.U.T.")
    }
    
    private func formReset(){
        if regionFee.count != 0 {
            contrySelection.setTitle(regionFee[0], for: UIControlState.normal)
        }
        country_select.selectedRow(inComponent: 0)
        newPassword.text = ""
        confirmPassword.text = ""
        phone_field.text = ""
        email_field.text = ""
        full_name.text = ""
    }
    
    private func setFinalAnySituation(showErrorBox: Bool, errorMsg:String){
        UIStatus.isHidden = !showErrorBox
        UIStatus.text = errorMsg
    }
    
    private func gettingUIStatus(isActive: Bool) {
        plabel.isHidden = isActive
        registro.isHidden = isActive
        inicio.isHidden = isActive
        progress.isHidden = !isActive
        status.text = (isActive ? "Un momento" : "¿Quieres comprar una entrada?")
    }
    
    @IBAction func search_run(_ sender: UIButton) {
        if ((runField.text?.count)! > 6 && (runField.text?.count)! < 15){
            formReset()
            sender.isEnabled = false
            runField.isEnabled = false
            secondRegisterID.isHidden = true
            registro.isEnabled = false
            guard let run_buscado = URL(string: "https://sendmepro.com/promo/index.php/prime/check_registered_user?run=\(runField.text!)") else {
                return
            }
            jsonClient.execJSONP(uri: run_buscado) {
                (jsp, error) in
                if let error = error {
                    self.setFinalAnySituation(showErrorBox: true, errorMsg: "Error de Conexión: \(error.localizedDescription)")
                }else if let jsp = jsp {
                    let error_request:Bool
                    var showError:Bool = false
                    var error_msg:String = ""
                    if ((jsp.value(forKey: "error_net")) != nil) {
                        error_request = jsp.value(forKey: "error_net") as! Bool
                    }else{
                        error_request = false
                    }
                    if (error_request || self.regionServerPhone != "CL"){
                        showError = true
                        error_msg = (self.regionServerPhone != "CL" ? "Debe ingresar manualmente los campos.":"Al parecer no es posible hacer una busqueda en estos momentos.")
                        self.secondRegisterID.isHidden = false
                        self.registro.isEnabled = true
                        self.full_name.isEnabled = true
                    }else{
                        self.full_name.isEnabled = false
                        let auto_name:String = jsp.value(forKey: "name") as! String
                        if (auto_name.count) > 0 {
                            self.full_name.text = auto_name
                            self.secondRegisterID.isHidden = false
                            self.registro.isEnabled = true
                        }else{
                            self.runField.becomeFirstResponder()
                            self.exDialog.regularUIDialog(tit: "Atención", titMsg: "El RUT ingresado no existe, por favor ingrese un RUT valido para continuar.") {
                                (dial) in
                                self.present(dial!, animated: true, completion: nil)
                            }
                        }
                    }
                    self.setFinalAnySituation(showErrorBox: showError, errorMsg: error_msg)
                }
                sender.isEnabled = true
                self.runField.isEnabled = true
            }
        }else{
            exDialog.regularUIDialog(tit: "Atención", titMsg: (regionServerPhone != "CL" ? "Ingrese su nombre de usuario para continuar.": "Por favor ingrese su RUT para comenzar con la busqueda.")) {
                (dial) in
                self.present(dial!, animated: true, completion: nil)
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func nextToComplete(_ sender: UITextField) {
        confirmPassword.becomeFirstResponder()
    }
    @IBAction func completedFunc(_ sender: UITextField) {
        
    }
    
    func session_verifier(user: String,pass: String){
        jsonClient.post_request_callback(urmode: "?mode=ln-re", params: ["run":String((user.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))) as Any, "psw":pass as Any]) {
            (hum,code) in
            if code != -1 {
                let feedback:Int8 = hum?.value(forKey: "isReady") as! Int8
                if feedback == 1 {
                    self.jsonClient.saveAndRemoveCredentials(isRemove: false, setCreds: ["USER":user,"PASS":pass])
                    self.runField.text = ""
                    self.formReset()
                    self.registro_UI.isHidden = true
                    self.secondRegisterID.isHidden = true
                    self.registro.setTitle("Registrarse", for: UIControlState.normal)
                    let sb:UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc:Reservas = sb.instantiateViewController(withIdentifier: "palcoGet") as! Reservas
                    //vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal
                    self.present(vc, animated: true, completion: nil)
                }else{
                    self.jsonClient.saveAndRemoveCredentials(isRemove: true, setCreds: [:])
                    if self.isReallyLoaded {
                        self.exDialog.regularUIDialog(tit: "Atención", titMsg: (feedback == 2 ? "Contraseña incorrecta o no valida, por favor intentelo de nuevo.":"Nombre de usuario inexistente.")) {
                            (dial) in
                            self.present(dial!, animated: true, completion: nil)
                        }
                    }else{
                        self.isReallyLoaded = true
                    }
                }
            }else{
                self.exDialog.regularUIDialog(tit: "Error de conexion", titMsg: "Los servidores estan muy ocupados para procesar su solicitud, por favor vuelva a intentarlo más tarde.") {
                    (dial) in
                    self.present(dial!, animated: true, completion: nil)
                }
            }
            self.gettingUIStatus(isActive: false)
        }
    }
    
    @IBAction func initialize_topay(_ sender: UIButton) {
        let logonServer = UIAlertController(title: "Iniciar Sesión", message: "Por favor ingrese los siguientes datos para continuar.", preferredStyle: .alert)
        var RUN : UITextField?
        var secretPass : UITextField?
        /*let forgottenAction = UIAlertAction(title: NSLocalizedString("¿Olvidaste tu contraseña?", comment: "Forgotten"), style: .default, handler: { _ in
            
        })*/
        let confirmAction = UIAlertAction(title: NSLocalizedString("Acceder", comment: "Logon"), style: .default, handler: { _ in
            var incomplete:Bool = false
            if (RUN?.text?.count)! < 6 || (secretPass?.text?.count)! < 7 {
                incomplete = true
            }
            if !incomplete {
                self.gettingUIStatus(isActive: true)
                self.session_verifier(user: (RUN?.text)!, pass: (secretPass?.text)!)
            }
        })
        logonServer.addTextField {
            (runfu) in runfu.placeholder = (self.regionServerPhone != "CL" ? "Username" : "R.U.T.")
            runfu.keyboardType = (self.regionServerPhone != "CL" ? UIKeyboardType.default:UIKeyboardType.numberPad)
            runfu.layoutMargins.bottom = 10
            runfu.autocorrectionType = UITextAutocorrectionType.no
            RUN = runfu
            confirmAction.isEnabled = (RUN?.text?.count)! < 7
        }
        logonServer.addTextField {
            (secrets) in secrets.placeholder = "Contraseña"
            secrets.keyboardType = .default
            secrets.layoutMargins.bottom = 10
            secrets.isSecureTextEntry = true
            secrets.returnKeyType = UIReturnKeyType.done
            secretPass = secrets
            confirmAction.isEnabled = (secretPass?.text?.count)! < 6
        }
        let abortAction = UIAlertAction(title: "Cancelar", style:.cancel, handler: nil)
        logonServer.addAction(abortAction)
        //logonServer.addAction(forgottenAction)
        logonServer.addAction(confirmAction)
        self.present(logonServer, animated: true, completion: nil)
    }
    
    @IBAction func register_action(_ sender: UIButton) {
        if (readyAndApply){
            plabel.text = "Al terminar, asegurese de revisar el formulario de registro de caracter obligatorio(*)."
            if (registro_UI.isHidden){
                registro_UI.isHidden = false
                registro.isEnabled = false
                registro.setTitle("Completar Registro", for: .normal)
            }else{
                var isValidForm:Bool = true
                var message:String = ""
                if (full_name.text?.count)! < 15 || (full_name.text?.count)! > 49 {
                    isValidForm = false
                    message = "Debe ingresar su nombre completo para proceder con el registro."
                    full_name.becomeFirstResponder()
                }else if !isValidEmail(testStr: email_field.text!) && (email_field.text?.count)! > 0{
                    isValidForm = false
                    message = "Por favor ingrese su correo electronico correctamente para proceder con el registro, si no lo quiere agregar dejelo en blanco."
                    email_field.becomeFirstResponder()
                }else if (phone_field.text?.count)! > 0{
                    if (phone_field.text?.count)! < 8{
                        isValidForm = false
                        message = "Por favor ingrese su numero telefonico o celular para el registro, si no lo quiere agregar dejelo en blanco."
                        phone_field.becomeFirstResponder()
                    }
                }else if (newPassword.text?.count)! < 7 {
                    isValidForm = false
                    message = "Por favor utilize una contraseña que pueda recordar para futuras reservas."
                    newPassword.becomeFirstResponder()
                }else if (newPassword.text != confirmPassword.text) {
                    isValidForm = false
                    message = "La contraseña que ingreso no coincide con la anterior, por favor intentelo nuevamente."
                    confirmPassword.becomeFirstResponder()
                }
                if isValidForm {
                    self.gettingUIStatus(isActive: true)
                    //INICIALIZAR REGISTRO FINAL
                    //Config
                    let values = ["value_0":(runField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!,"value_1":full_name.text!,"value_2":(email_field.text?.count)! > 0 ? (email_field.text?.lowercased())!:"","value_3":(phone_field.text?.count)! > 0 ? Int(phone_field.text!)!:"","value_4":0,"value_5":regionSelected,"SECRET":newPassword.text!,"NON-LOGIN":true,"cuvalue":[false,false,false,true,true,true,false]] as [String : Any]
                    jsonClient.post_request_callback(urmode: "?mode=exp", params: ["launch":"regularRegistration","AM":values]) {
                        (anp, errInt) in
                        if errInt == 0{
                            
                        } else{
                            let raja:Int8 = anp?.value(forKey: "error_int") as! Int8
                            if (raja >= 1) {
                                self.jsonClient.saveAndRemoveCredentials(isRemove: false, setCreds: ["USER":values["value_0"]!,"PASS":values["SECRET"]!])
                                
                                self.status.text = "Registro Completado"
                                self.isReallyLoaded = false
                                //self.readyAndApply = false
                                self.systemJSONLoad()
                            }else{
                                self.gettingUIStatus(isActive: false)
                                self.exDialog.regularUIDialog(tit: "Atención", titMsg: "La persona ya se encuentra registrada, si no recuerda su contraseña anterior, solicite una recuperación de contraseña") {
                                    (dial) in
                                    self.present(dial!, animated: true, completion: nil)
                                }
                            }
                        }
                        
                    }
                }else{
                    exDialog.regularUIDialog(tit: "Registro Incompleto", titMsg: message) {
                        (dial) in
                        self.present(dial!, animated: true, completion: nil)
                    }
                    confirmPassword.text = ""
                }
            }
        }else{
            systemJSONLoad()
            systemReloadingDistroFunction(errorAppear: false, errorStatus: "")
        }
    }
    
    @IBAction func maxlength_full_name(_ sender: UITextField) {
        sender.text = sender.text?.uppercased()
        if (sender.text?.count)! > 49 {
            let ojo = sender.text!
            let indexStartOfText = ojo.index(ojo.startIndex, offsetBy: 1)
            sender.text = String(ojo[indexStartOfText...])
        }
    }
    func textField_rangeLimiter(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 10
    }
    
    private func systemJSONLoad(){
        //CARGA REMOTA DE COMUNAS
        if regionFee.count == 0 {
            guard let country_loader = URL(string: "https://elgrancircoderuperto.cl/app/pub_access/?mode=country") else {
                return
            }
            jsonClient.execJSONP(uri: country_loader) {
                (jepp, error) in
                if let error = error {
                    self.systemReloadingDistroFunction(errorAppear: true, errorStatus: error.localizedDescription)
                }
                else if let jepp = jepp {
                    let country_arr:[String] = jepp.value(forKey: "country_select") as! [String]
                    self.regionFee = country_arr
                    self.readyAndApply = true
                }
            }
        }
        //CARGA AUTOMATICA SI ES QUE EXISTE UNA SESION INTERNA
        if UserDefaults.standard.object(forKey: "privateCred") != nil {
            let getPrivCred:[String:Any] = UserDefaults.standard.object(forKey: "privateCred") as! [String:Any]
            self.session_verifier(user: getPrivCred["USER"] as! String, pass: getPrivCred["PASS"] as! String)
        } else {
            self.gettingUIStatus(isActive: false)
            self.isReallyLoaded = true
        }
    }
    
    private func systemReloadingDistroFunction(errorAppear: Bool, errorStatus: String){
        plabel.text = (errorAppear ? "Oops, estas teniendo problemas de conexión, por favor vuelve a intentarlo más tarde" : "Primero necesitamos que te registres para continuar, si ya eres 'Cliente Habitual' por favor inicia sesión.")
        status.text = (errorAppear ? "Error de Conexión\n\(errorStatus)" : "Reconectando")
        plabel.isHidden = !errorAppear
        progress.isHidden = errorAppear
        registro.isHidden = !errorAppear
        registro.setTitle(errorAppear ? "Volver a intentarlo" : "Registrarse", for: [.normal])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        systemJSONLoad()
    }
}

extension distroPurchase: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return regionFee.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (!pickerView.isHidden){
            regionSelected = row
            contrySelection.setTitle(regionFee[regionSelected], for: UIControlState.normal)
            contrySelection.isHidden = false
            pickerView.isHidden = true
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return regionFee[row]
    }
}
